//var fs  = require('fs');
var obj = JSON.parse({"widget": {
    "debug": "on",
    "window": {
        "title": "Sample Konfabulator Widget",
        "name": "main_window",
        "width": 500,
        "height": 500
    },
    "image": { 
        "src": "Images/Sun.png",
        "name": "sun1",
        "hOffset": 250,
        "vOffset": 250,
        "alignment": "center"
    },
    "text": {
        "data": "Click Here",
        "size": 36,
        "style": "bold",
        "name": "text1",
        "hOffset": 250,
        "vOffset": 100,
        "alignment": "center",
        "onMouseUp": "sun1.opacity = (sun1.opacity / 100) * 90;"
    }
}});
    
    (function create(folder, o) {
        for (var key in o) {
            if ( typeof o[key] === 'object' && o[key] !== null) {
                console.log('folder created : ' + (folder + key))
                //fs.mkdir(folder + key, function() {
                    if (Object.keys(o[key]).length) {
                        create(folder + key + '/', o[key]);
                    }
                //});
            } else {
                console.log('file created : ' + (folder + key + (o[key] === null ? '' : '.' + o[key])))
                //fs.writeFile(folder + key + (o[key] === null ? '' : '.' + o[key]));
            }
        }
    })('/', obj);